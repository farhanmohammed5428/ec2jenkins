if [ "$State" = "Start" ]
then
aws ec2 start-instances --instance-ids $InstanceID
echo Instance $InstanceID Started
elif [ "$State" = "Stop" ]
then
aws ec2 stop-instances --instance-ids $InstanceID
echo Instance $InstanceID Stopped
 fi